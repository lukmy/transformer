#!/usr/bin/env python
import json
import os
import time
from datetime import datetime

import fire
import xlrd
from elasticsearch import Elasticsearch


class Transformer(object):

    def _process_format_transformation(self, data, target_format):
        if target_format in {'wgs', 'wgs84'}:
            # from evil_transform import gcj2wgs as transform
            from utils.evil_transform import bd2wgs as transform
        elif target_format in {'gcj'}:
            # from evil_transform import gcj2bd as transform
            from utils.evil_transform import bd2gcj as transform
        else:
            raise ValueError('Invalid target format')

        new_data = []
        for d in data:
            # d.latitude, d.longitude = transform(d.latitude, d.longitude)
            d['latitude'], d['longitude'] = transform(d['latitude'], d['longitude'])
            new_data.append(d)
        return new_data

    def _transform_file(self, filename, target_format='wgs84'):
        with open(filename, 'r') as f:
            data = [json.loads(line) for line in f.readlines()]

        return self._process_format_transformation(data, target_format)

    def transform_file(self, filename, target_format='wgs84'):
        data = self._transform_file(filename, target_format)
        new_filename = u'{0}_{1}'.format(target_format, filename)
        with open(new_filename, 'w') as f:
            f.writelines([
                json.dumps(d, ensure_ascii=False).encode('UTF-8') + "\n" for d in data
            ])


class Uploader(object):

    def _post_processing(self, data):
        for d in data:
            try:
                d['yeara'] = int(d['yeara'])
                d['yearb'] = int(d['yearb'])
                d['location'] = {
                    'lat': d['latitude'],
                    'lon': d['longitude']
                }
                d['timestamp'] = datetime.utcnow()
            except:
                continue
            else:
                yield d

    def transform_file_and_upload_to_elasticsearch(self, filename,
                                                   target_format='wgs84'):
        t = Transformer()
        data = t._transform_file(filename, target_format)

        es = Elasticsearch('localhost:9200')

        for v in self._post_processing(data):
            es.index(index='lianjia-info', doc_type='v1', body=v)
            print "sending {0}".format(v)
            # time.sleep(1)


class MobikeCrawler(object):

    def _post_processing(self, data):
        for d in data:
            try:
                from utils.evil_transform import gcj2wgs as transform

                lat, lon = transform(d['distY'], d['distX'])
                d['location'] = {
                    'lat': lat,
                    'lon': lon
                }
                d['timestamp'] = datetime.utcnow()

                del d['distance']
            except:
                continue
            else:
                yield d

    def fetch_mobike_data_and_upload_to_elasticsearch(self):
        from utils.mobike_crawler import get_bikes_for_all_areas

        bikes = get_bikes_for_all_areas()

        es = Elasticsearch('localhost:9200')

        for v in self._post_processing(bikes):
            es.index(index='mobike-info', doc_type='v1', body=v)
            print 'sending {0}'.format(v)

    def fetch_mobike_data_and_print(self):
        from utils.mobike_crawler import get_bikes_for_all_areas

        for b in get_bikes_for_all_areas():
            print b

    def fetch_mobike_data_and_write_to_file(self, filename):
        from utils.mobike_crawler import get_bikes_for_all_areas

        with open(filename, 'w') as f:
            for b in get_bikes_for_all_areas():
                f.write(u'{0}\n'.format(json.dumps(b)))

    def _new_jsonl_f(self, directory, location, timestamp):
        from os import path
        return '{0}-{1}.jsonl'.format(path.join(directory, location), timestamp)

    def fetch_mobike_data_and_write_to_directory(self, directory, timestamp):
        from utils.mobike_crawler import get_bikes_for_all_areas

        for location, bikes in get_bikes_for_all_areas().iteritems():
            with open(self._new_jsonl_f(directory, location, timestamp), 'w') as f:
                for b in bikes:
                    f.write(u'{0}\n'.format(json.dumps(b)))
            # for b in bikes:
            #     print b


if __name__ == '__main__':
    # fire.Fire(Transformer)
    # fire.Fire(Uploader)
    fire.Fire(MobikeCrawler)
