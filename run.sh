#!/bin/bash

mkdir -p data/mobike
source /Users/lorenzo/.pyenv/versions/lab-py2/bin/activate
python transformers.py fetch_mobike_data_and_write_to_directory --directory data/mobike --timestamp $(date +%Y-%m-%d-%H:%M)
