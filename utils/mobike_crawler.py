import random
import requests
import time
import traceback

from retrying import retry


@retry(stop_max_attempt_number=3, wait_fixed=1000)
def get_bikes(lat, lon):
    url = 'https://mwx.mobike.com/mobike-api/rent/nearbyBikesInfo.do'
    headers = {
        # "time": "1492874552373",
        # "Content-Length": "93",
        "Accept": "*/*",
        "eption": "00c4c",
        "mobileNo": "18692165516",
        "open_src": "list",
        "accesstoken": "564876b029f8d6a01373df12d0d8e5cd",
        "platform": "3",
        "Accept-Language": "zh-cn",
        "citycode": "021",
        "User-Agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) Mobile/14E304 MicroMessenger/6.5.7 NetType/WIFI Language/zh_CN",
        "lang": "zh",
        "Referer": "https://servicewechat.com/wx80f809371ae33eda/22/page-frame.html",
        "Accept-Encoding": "gzip, deflate",
        "Connection": "keep-alive",
        "Content-Type": "application/x-www-form-urlencoded"
    }
    data = u'errMsg=getLocation%3Aok&speed=-1&latitude={0}&longitude={1}&accuracy=165&citycode=021'.format(lat, lon)
    resp = requests.post(url, headers=headers, data=data)

    try:
        objects = resp.json()['object']
    except Exception as e:
        objects = {}
        print 'Failed to parse response, exception: {1}, text: {0}'.format(resp.text, e)
    finally:
        return objects, resp


class GridBikeCrawler(object):
    def __init__(self, lon_l, lon_r, lat_h, lat_l, lon_intervals, lat_intervals):
        self.lon_l = lon_l
        self.lon_r = lon_r
        self.lat_h = lat_h
        self.lat_l = lat_l

        self.lon_intervals = lon_intervals
        self.lat_intervals = lat_intervals

    def get_bikes(self):
        lon_total = self.lon_r - self.lon_l
        lat_total = self.lat_h - self.lat_l

        lon_0 = self.lon_l
        lat_0 = self.lat_l

        points = [(lat_0 + lat_total * i / self.lat_intervals, lon_0 + lon_total * j / self.lon_intervals)
                  for i in xrange(self.lat_intervals) for j in xrange(self.lon_intervals)]
        random.shuffle(points)

        bikes = []
        for p in points:
            try:
                bikes_near_point, _ = get_bikes(p[0], p[1])
                bikes += bikes_near_point
            except Exception:
                traceback.print_exc()

            time.sleep(1)

            print 'finished fetch point {0}'.format(p)

        bikes = {b['bikeIds']: b for b in bikes}.values()

        return bikes


class PointCrawler(GridBikeCrawler):
    def __init__(self, lon, lat):
        super(PointCrawler, self).__init__(lon, lon, lat, lat, 1, 1)


def get_bikes_for_all_areas():
    areas = {
        'TongjiUniversity': GridBikeCrawler(
            121.4961653370184, 121.5083588645905,
            31.28759699164695, 31.27888876871922, 6, 4),
        'KIC': GridBikeCrawler(121.5111737124455, 121.5150149218655,
                               31.3081064047962, 31.30465132413224, 2, 3),
        'TongjiUniversitySubwayStation': PointCrawler(121.5065440440482, 31.28238242881696),
        'TongjiVillage': GridBikeCrawler(121.5065440440482, 121.5131208961397,
                                         31.28600444572531, 31.2811249515192, 3, 2),
        'Wujiaochang': PointCrawler(121.5140747232001, 31.29863522028849),
        'BauhiniaSquare': PointCrawler(121.5171793556302, 31.27491568920659)
    }

    all_bikes = {}
    for k, v in areas.iteritems():
        print('Fetching bikes in {0}'.format(k))
        all_bikes[k] = v.get_bikes()
    return all_bikes
