completion:
	python transformers.py -- --completion

pip-base:
	pip install -r requirements/base.txt

pip: pip-base
	pip install -r requirements/dev.txt

docker-up:
	sudo docker-compose up

bootstrap: pip docker-up
